//! # Rusty Numbers
//!
//! Playin' with Numerics in Rust
#![forbid(unsafe_code)]
#![no_std]
#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::similar_names)]

#[cfg(all(feature = "alloc", not(feature = "std")))]
#[macro_use]
extern crate alloc;

#[cfg(feature = "std")]
#[macro_use]
extern crate std;

pub mod bigint;
pub mod num;
pub mod rational;
