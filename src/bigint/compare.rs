#[cfg(all(feature = "alloc", not(feature = "std")))]
extern crate alloc;
#[cfg(all(feature = "alloc", not(feature = "std")))]
use alloc::string::*;
#[cfg(all(feature = "alloc", not(feature = "std")))]
use alloc::vec::*;
use core::cmp::{Ordering, PartialEq, PartialOrd};
use core::prelude::v1::*;

use super::BigInt;

macro_rules! impl_ord_literal {
    ($($prim: ty),+) => {
        $(
            impl PartialEq<$prim> for BigInt {
                #[must_use]
                fn eq(&self, other: &$prim) -> bool {
                    self == &BigInt::new(*other)
                }
            }

            impl PartialEq<BigInt> for $prim {
                #[must_use]
                fn eq(&self, other: &BigInt) -> bool {
                    &BigInt::new(*self) == other
                }
            }

            impl PartialOrd<$prim> for BigInt {
                #[must_use]
                fn partial_cmp(&self, other: &$prim) -> Option<Ordering> {
                    self.partial_cmp(&BigInt::new(*other))
                }
            }

            impl PartialOrd<BigInt> for $prim {
                #[must_use]
                fn partial_cmp(&self, other: &BigInt) -> Option<Ordering> {
                    (&BigInt::new(*self)).partial_cmp(other)
                }
            }
        )+
    };
}

// Implement PartialEq and PartialOrd to compare against BigInt values
impl_ord_literal!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128);
