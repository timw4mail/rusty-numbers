//! \[WIP\] Arbitrarily large integers
mod compare;
mod from;
pub use compare::*;
pub use from::*;

use crate::num::FracOp;
use crate::num::Sign::{self, Negative, Positive};

#[cfg(all(feature = "alloc", not(feature = "std")))]
extern crate alloc;
#[cfg(all(feature = "alloc", not(feature = "std")))]
use alloc::string::*;
#[cfg(all(feature = "alloc", not(feature = "std")))]
use alloc::vec::*;
use core::cmp::{Ordering, PartialEq, PartialOrd};
use core::convert::TryInto;
use core::ops::{
    Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Not, Rem, RemAssign, Sub, SubAssign,
};
use core::prelude::v1::*;
#[cfg(feature = "std")]
use std::prelude::v1::*;

#[derive(Clone, Debug, PartialEq)]
/// The representation of a `BigInt`
pub struct BigInt {
    inner: Vec<usize>,
    sign: Sign,
}

/// Create a [`BigInt`](bigint/struct.BigInt.html) type with signed or unsigned number literals
#[macro_export]
macro_rules! bint {
    ($w:literal) => {
        $crate::bigint::BigInt::new($w)
    };
    (- $x:literal) => {
        $crate::bigint::BigInt::new(-$x)
    };
}

impl Default for BigInt {
    fn default() -> Self {
        Self {
            inner: vec![0],
            sign: Sign::default(),
        }
    }
}

impl BigInt {
    /// Create a new Bigint, of value 0
    ///
    /// In most cases, you probably want to use [new](`BigInt::new`) instead
    #[must_use]
    pub fn zero() -> Self {
        Self::default()
    }

    /// Create a new `BigInt` from an existing Rust primitive type
    pub fn new(initial: impl Into<BigInt>) -> Self {
        initial.into()
    }

    fn empty() -> Self {
        Self {
            inner: Vec::new(),
            sign: Sign::Positive,
        }
    }

    /// Create a new `BigInt`, with the specified inner capacity
    #[must_use]
    pub fn with_capacity(size: usize) -> Self {
        Self {
            inner: Vec::with_capacity(size),
            sign: Positive,
        }
    }

    /// Remove digits that are zero from the internal representation.
    ///
    /// Similar to 007 -> 7 in base 10
    pub fn trim_zeros(&mut self) {
        let current_len = self.inner.len();
        if current_len < 2 {
            return;
        }

        let mut trailing_zeros = 0_usize;
        for val in self.inner.iter().rev() {
            if *val != 0 {
                break;
            }

            trailing_zeros += 1;
        }

        // Always keep at least one digit
        if trailing_zeros == current_len {
            trailing_zeros -= 1;
        }

        let new_len = current_len - trailing_zeros;

        self.inner.truncate(new_len);
    }

    /// Remove unused digits, and shrink the internal vector
    pub fn shrink_to_fit(&mut self) {
        self.trim_zeros();
        self.inner.shrink_to_fit();
    }

    /// Convert a `&str` or a `String` representing a number in the specified radix to a Bigint.
    ///
    /// For radix 10, use the `new` or `from` associated function instead.
    ///
    /// Radix must be between 1 and 36, inclusive, with radix higher
    /// than 11 represented by A-Z
    ///
    /// Only alphanumeric characters are considered, so exponents and
    /// other forms are not parsed
    ///
    /// # Panics
    /// * If radix is not between 1 and 36 inclusive
    /// * Some branches are not yet implemented
    pub fn from_str<T: ToString + ?Sized>(s: &T, radix: usize) -> BigInt {
        // Two lines due to borrow checker complaints
        let input = s.to_string().to_ascii_uppercase();
        let input = input.trim();

        assert!(
            radix > 0 && radix <= 36,
            "Radix must be between 1 and 36, inclusive. Given radix: {}",
            radix
        );

        // In base 1, number of place values = total value
        if radix == 1 {
            let mut raw_digits: Vec<usize> = Vec::with_capacity(input.len());
            for char in input.chars() {
                match char {
                    '1' => raw_digits.push(1),
                    _ => continue,
                }
            }

            return BigInt::new(raw_digits.len());
        }

        // If the number fits in a usize, try the easy way
        if let Ok(easy_res) = usize::from_str_radix(input, radix.try_into().unwrap()) {
            return BigInt::new(easy_res);
        }

        // TODO: consider parsing out the error, to tell if the
        // parsed result is valid for the base

        // Convert each digit to it's decimal representation
        let mut raw_digits: Vec<usize> = Vec::with_capacity(input.len());
        for maybe_digit in input.chars() {
            #[allow(clippy::cast_possible_truncation)]
            match maybe_digit.to_digit(radix as u32) {
                Some(d) => raw_digits.push(d as usize),
                None => continue,
            }
        }

        // Calculate the decimal value by calculating the value
        // of each place value
        todo!();
    }

    /// Get the larger number of digits when comparing two `BigInt`s.
    /// This is helpful to determine sizing for a `BigInt` after
    /// a numeric operation.
    fn get_ceil_digit_count(a: &Self, b: &Self) -> usize {
        let a_digits = a.inner.len();
        let b_digits = b.inner.len();

        if a_digits == 0 && b_digits == 0 {
            return 1;
        }

        if b_digits > a_digits {
            b_digits
        } else {
            a_digits
        }
    }

    /// Determine the output sign given the two input signs and operation
    fn get_sign(a: &Self, b: &Self, op: FracOp) -> Sign {
        // -a + -b = -c
        if op == FracOp::Addition && a.sign == Negative && b.sign == Negative {
            return Negative;
        }

        // a - -b = c
        if op == FracOp::Subtraction && a.sign == Positive && b.sign == Negative {
            return Positive;
        }

        if a.sign == b.sign {
            Positive
        } else {
            Negative
        }
    }

    /// Normal primitive multiplication
    fn prim_mul(self, rhs: &Self, digits: usize) -> Self {
        let mut out = BigInt::with_capacity(digits);

        let mut carry = 0_usize;
        for i in 0..digits {
            let a = *self.inner.get(i).unwrap_or(&0_usize);
            let b = *rhs.inner.get(i).unwrap_or(&0_usize);

            if a == 0 || b == 0 {
                out.inner.push(0);
                continue;
            }

            let (res, overflowed) = a.overflowing_mul(b);

            if overflowed {
                todo!("Multiplication overflow not yet implemented")
            } else {
                let (res, overflowed) = res.overflowing_add(carry);

                out.inner.push(res);
                carry = if overflowed { 1 } else { 0 };
            }
        }

        out.sign = Self::get_sign(&self, rhs, FracOp::Other);
        out.shrink_to_fit();

        out
    }
}

impl Add for BigInt {
    type Output = Self;

    #[must_use]
    fn add(self, rhs: Self) -> Self::Output {
        // If the sign of one input differs,
        // subtraction is equivalent
        if self.sign == Negative && rhs.sign == Positive {
            return rhs - -self;
        } else if self.sign == Positive && rhs.sign == Negative {
            return self - -rhs;
        }

        let digits = Self::get_ceil_digit_count(&self, &rhs) + 1;
        let mut out = BigInt::with_capacity(digits);

        let mut carry = 0_usize;
        for i in 0..digits {
            let a = *self.inner.get(i).unwrap_or(&0_usize);
            let b = *rhs.inner.get(i).unwrap_or(&0_usize);

            let (res, overflowed) = a.overflowing_add(b);
            if res == 0 && !overflowed {
                out.inner.push(res + carry);
                carry = 0;
                continue;
            }

            if overflowed {
                out.inner.push(res + carry);
                carry = 1;
            } else if res < usize::MAX {
                out.inner.push(res + carry);
                carry = 0;
            } else {
                out.inner.push(0_usize);
                carry = 1;
            }
        }

        out.sign = Self::get_sign(&self, &rhs, FracOp::Addition);

        out.trim_zeros();

        out
    }
}

impl Sub for BigInt {
    type Output = Self;

    #[must_use]
    fn sub(self, rhs: Self) -> Self::Output {
        let digits = Self::get_ceil_digit_count(&self, &rhs);
        let mut out = BigInt::with_capacity(digits);

        // Handle cases where addition makes more sense
        if self.sign == Positive && rhs.sign == Negative {
            return self + -rhs;
        } else if self.sign == Negative && rhs.sign == Positive {
            return -(rhs + -self);
        }

        let mut borrow = 0_usize;
        for i in 0..digits {
            let a = *self.inner.get(i).unwrap_or(&0_usize);
            let b = *rhs.inner.get(i).unwrap_or(&0_usize);

            if a >= borrow && (a - borrow) >= b {
                // This is the easy way, no additional borrowing or underflow
                let res = a - b - borrow;

                out.inner.push(res);
                borrow = 0;
            } else {
                // To prevent overflow, the max borrowed value is
                // usize::MAX (place-value - 1). The rest of the borrowed value
                // will be added on afterwords.
                // In base ten, this would be like:
                // 15 - 8 = (9 - 8) + (5 + 1)
                let rem = (a + 1) - borrow;
                let res = (usize::MAX - b) + rem;
                out.inner.push(res);

                borrow = 1;
            }
        }

        out.sign = Self::get_sign(&self, &rhs, FracOp::Subtraction);

        out.trim_zeros();

        out
    }
}

impl Mul for BigInt {
    type Output = Self;

    #[must_use]
    fn mul(self, rhs: Self) -> Self::Output {
        // Multiplication can result in twice the number of digits
        let out_digits = Self::get_ceil_digit_count(&self, &rhs) * 2;

        self.prim_mul(&rhs, out_digits)
    }
}

impl Div for BigInt {
    type Output = Self;

    #[must_use]
    fn div(self, _rhs: Self) -> Self::Output {
        todo!("Division is not yet implemented")
    }
}

impl Rem for BigInt {
    type Output = Self;

    #[must_use]
    fn rem(self, _rhs: Self) -> Self::Output {
        todo!("Modulus is not yet implemented")
    }
}

impl AddAssign for BigInt {
    fn add_assign(&mut self, rhs: Self) {
        let this = core::mem::replace(self, BigInt::zero());
        *self = this + rhs;
    }
}

impl SubAssign for BigInt {
    fn sub_assign(&mut self, rhs: Self) {
        let this = core::mem::replace(self, BigInt::zero());
        *self = this - rhs;
    }
}

impl MulAssign for BigInt {
    fn mul_assign(&mut self, rhs: Self) {
        let this = core::mem::replace(self, BigInt::zero());
        *self = this * rhs;
    }
}

impl DivAssign for BigInt {
    fn div_assign(&mut self, rhs: Self) {
        let this = core::mem::replace(self, BigInt::zero());
        *self = this / rhs;
    }
}

impl RemAssign for BigInt {
    fn rem_assign(&mut self, rhs: Self) {
        let this = core::mem::replace(self, BigInt::zero());
        *self = this % rhs;
    }
}

impl Neg for BigInt {
    type Output = Self;

    /// Flip the sign of the current `BigInt` value
    #[must_use]
    fn neg(mut self) -> Self::Output {
        self.sign = !self.sign;

        self
    }
}

impl Not for BigInt {
    type Output = Self;

    /// Do a bitwise negation of every digit's value
    fn not(self) -> Self::Output {
        let mut flipped: Vec<usize> = Vec::with_capacity(self.inner.len());

        for val in &self.inner {
            let rev = !*val;
            flipped.push(rev);
        }

        BigInt {
            sign: self.sign,
            inner: flipped,
        }
    }
}

impl PartialOrd for BigInt {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // The signs differ
        if self.sign != other.sign {
            // If the signs are different, the magnitude doesn't matter
            // unless the value is zero on both sides
            return if self.eq(&0) && other.eq(&0) {
                Some(Ordering::Equal)
            } else {
                self.sign.partial_cmp(&other.sign)
            };
        }

        // Everything is the same
        if self.inner == other.inner {
            return Some(Ordering::Equal);
        }

        // The number of place values differs
        if self.inner.len() != other.inner.len() {
            return if self.inner.len() > other.inner.len() {
                Some(Ordering::Greater)
            } else {
                Some(Ordering::Less)
            };
        }

        // At this point the sign is the same, and the number of place values is equal,
        // so compare the individual place values (from greatest to least) until they
        // are different. At this point, the digits can not all be equal.
        for i in (0_usize..self.inner.len()).rev() {
            let compare = self.inner[i].cmp(&other.inner[i]);
            if compare != Ordering::Equal {
                return Some(compare);
            }
        }

        unreachable!();
    }
}

#[cfg(test)]
#[cfg(not(tarpaulin_include))]
mod tests {
    use super::*;

    const RADIX: u128 = usize::MAX as u128 + 1;
    const I_RADIX: i128 = usize::MAX as i128 + 1;

    #[test]
    fn sanity_checks() {
        let int = BigInt::new(45u8);
        assert_eq!(int.inner[0], 45usize)
    }

    #[test]
    fn test_macro() {
        let a = bint!(75);
        let b = BigInt::new(75);
        assert_eq!(a, b);

        let a = bint!(-75);
        let b = BigInt::new(-75);
        assert_eq!(a, b);
    }

    #[test]
    fn test_trim_zeros() {
        let mut lots_of_leading = BigInt {
            inner: vec![1, 0, 0, 0, 0, 0, 0, 0, 0],
            sign: Positive,
        };

        lots_of_leading.trim_zeros();

        assert_eq!(BigInt::new(1), lots_of_leading);
    }

    #[test]
    fn test_add() {
        // MAX is 2^Bitsize - 1,
        // so the sum should be
        // [MAX -1, 1]
        // Compare base 10: 9 + 9 = 18
        let a = BigInt::new(usize::MAX);
        let b = BigInt::new(usize::MAX);

        let sum = a + b;

        assert_eq!(
            sum.inner[0],
            usize::MAX - 1,
            "least significant place should be MAX - 1"
        );
        assert_eq!(sum.inner[1], 1usize, "most significant place should be 1");

        let a = BigInt::new(usize::MAX);
        let b = BigInt::new(1usize);
        let sum = a + b;
        assert_eq!(sum.inner[0], 0_usize);
        assert_eq!(sum.inner[1], 1usize);

        let a = BigInt::new(10);
        let b = -BigInt::new(5);
        let sum = a + b;
        assert_eq!(sum.inner[0], 5usize);
        assert_eq!(sum.sign, Positive);

        let a = -BigInt::new(5);
        let b = BigInt::new(10);
        let sum = a + b;
        assert_eq!(sum.inner[0], 5usize);
        assert_eq!(sum.sign, Positive);
    }

    #[test]
    fn test_add_assign() {
        let mut a = BigInt::new(usize::MAX);
        let b = BigInt::new(usize::MAX);

        a += b;

        assert_eq!(
            a.inner[0],
            usize::MAX - 1,
            "least significant place should be MAX - 1"
        );
        assert_eq!(a.inner[1], 1usize, "most significant place should be 1");
    }

    #[test]
    fn test_sub() {
        let a = BigInt::new(usize::MAX);
        let b = BigInt::new(u16::MAX);

        let diff = a - b;
        assert_eq!(diff.clone().inner[0], usize::MAX - u16::MAX as usize);
        assert_eq!(diff.inner.len(), 1);

        let a = BigInt::new(5);
        let b = -BigInt::new(3);
        let diff = a - b;
        assert_eq!(diff.sign, Positive);
        assert_eq!(diff.inner[0], 8usize);

        let a = -BigInt::new(5);
        let b = BigInt::new(3);
        let diff = a - b;
        assert_eq!(diff.sign, Negative);
        assert_eq!(diff.inner[0], 8usize);
    }

    #[test]
    fn test_sub_borrow() {
        let a = BigInt {
            inner: vec![0, 1],
            sign: Positive,
        };

        let b = BigInt::new(2);
        let diff = a - b;
        assert_eq!(diff.clone().inner.len(), 1, "{:#?}", diff.clone());
        assert_eq!(diff.inner[0], usize::MAX - 1);
    }

    #[test]
    fn test_sub_assign() {
        let mut a = BigInt {
            inner: vec![1, 0, 1],
            sign: Positive,
        };
        let b = BigInt::new(2);

        a -= b;

        assert_eq!(a.inner, vec![usize::MAX, usize::MAX]);
    }

    #[test]
    fn test_mul() {
        let a = BigInt::new(65536);
        let b = BigInt::new(4);

        let product = a * b;
        assert_eq!(product.inner[0], 65536usize * 4);
    }

    #[test]
    fn test_mul_signs() {
        let a = BigInt::new(2);
        let b = BigInt::new(-2);
        let product = a * b;
        assert_eq!(product.inner[0], 4usize);
        assert_eq!(product.sign, Negative);

        let a = -BigInt::new(2);
        let b = BigInt::new(2);
        let product = a * b;
        assert_eq!(product.inner[0], 4usize);
        assert_eq!(product.sign, Negative);

        let a = BigInt::new(-2);
        let b = BigInt::new(-2);
        let product = a * b;
        assert_eq!(product.inner[0], 4usize);
        assert_eq!(product.sign, Positive);

        let a = BigInt::new(2);
        let b = BigInt::new(2);
        let product = a * b;
        assert_eq!(product.inner[0], 4usize);
        assert_eq!(product.sign, Positive);
    }

    #[test]
    #[should_panic]
    fn test_mul_overflow() {
        let a = BigInt::new(usize::MAX);
        let b = BigInt::new(5);

        let _product = a * b;
    }

    #[test]
    #[should_panic]
    fn test_mul_assign_overflow() {
        let mut a = BigInt::new(usize::MAX);
        let b = BigInt::new(5);

        a *= b;
    }

    #[test]
    #[should_panic]
    fn test_div() {
        let a = BigInt::new(128);
        let b = BigInt::new(32);

        let _quotient = a / b;
    }

    #[test]
    #[should_panic]
    fn test_div_assign() {
        let mut a = BigInt::new(128);
        let b = BigInt::new(32);

        a /= b;
    }

    #[test]
    #[should_panic]
    fn test_rem() {
        let a = BigInt::new(5);
        let b = BigInt::new(2);

        let _rem = a % b;
    }

    #[test]
    #[should_panic]
    fn test_rem_assign() {
        let mut a = BigInt::new(5);
        let b = BigInt::new(2);

        a %= b;
    }

    #[test]
    fn test_zeros() {
        let a = BigInt::zero();
        let b = BigInt::zero();

        let c = a.clone() - b.clone();
        assert_eq!(a.clone(), b.clone());
        assert_eq!(c, a.clone());

        let c = a.clone() + b.clone();
        assert_eq!(a.clone(), b.clone());
        assert_eq!(c, a.clone());
    }

    #[test]
    fn test_not() {
        let a = BigInt::new(0_u8);
        let b = !a;

        assert_eq!(b.inner[0], usize::MAX);
    }

    #[test]
    fn test_partial_eq() {
        let a = 12345_u16;
        let b = BigInt::new(a);

        assert!(a.eq(&b));
        assert!(b.eq(&a));
    }

    #[test]
    fn test_partial_ord() {
        let a = 12345_u32;
        let b = BigInt::new(a);
        let c = 3_u8;

        assert_eq!(a.partial_cmp(&b), Some(Ordering::Equal));
        assert_eq!(c.partial_cmp(&b), Some(Ordering::Less));
        assert_eq!(b.partial_cmp(&c), Some(Ordering::Greater));

        assert!(bint!(-32) < bint!(3));
        assert!(bint!(3) > bint!(-32));
        assert!(bint!(152) > bint!(132));
        assert_eq!(bint!(123), bint!(123));
    }

    #[test]
    fn test_from() {
        // Signed numbers
        assert_eq!(-BigInt::new(2), BigInt::new(-2));

        // Larger than usize
        assert_eq!(BigInt::new(45_u128), BigInt::new(45_usize));
    }

    #[test]
    fn test_from_large_unsigned() {
        let big_num: u128 = 9 * RADIX + 8;
        let res = BigInt::new(big_num);

        assert_eq!(res.sign, Sign::Positive, "{:#?}", res);
        assert_eq!(res.inner[0], 8_usize, "{:#?}", res);
        assert_eq!(res.inner[1], 9_usize, "{:#?}", res);
    }

    #[test]
    fn test_from_large_signed() {
        let big_num: i128 = 2 * I_RADIX + 3;
        let res = BigInt::new(-big_num);

        assert_eq!(res.sign, Sign::Negative, "{:#?}", res);
        assert_eq!(res.inner[0], 3_usize, "{:#?}", res);
        assert_eq!(res.inner[1], 2_usize, "{:#?}", res);
    }

    #[test]
    #[should_panic]
    fn test_from_str_large() {
        let str = "ZYXWVUTSRQPONMLKJIHGFEDCBA987654321";
        let _ = BigInt::new(str);
    }

    #[test]
    fn test_from_str_small() {
        let str = "012345";
        let num = BigInt::new(str);
        assert_eq!(num.inner[0], 12345_usize);
    }

    #[test]
    #[should_panic]
    fn test_from_string_large() {
        let str = String::from("ZYXWVUTSRQPONMLKJIHGFEDCBA987654321");
        let _ = BigInt::new(str);
    }

    #[test]
    fn test_from_string_small() {
        let str = String::from("012345");
        let num = BigInt::new(str);
        assert_eq!(num.inner[0], 12345_usize);
    }

    #[test]
    fn test_from_str_radix_1() {
        let s = "1".repeat(32);
        let num = BigInt::from_str(&s, 1);
        assert_eq!(num.inner[0], 32_usize);
    }

    #[test]
    fn test_from_str_radix_1_with_zeroes() {
        let ones = "1".repeat(32);
        let zeroes = "0".repeat(24);
        let s = ones + &zeroes;

        let num = BigInt::from_str(&s, 1);
        assert_eq!(num.inner[0], 32_usize);
    }

    #[test]
    #[should_panic]
    fn test_from_str_radix_invalid() {
        let _ = BigInt::from_str("foobar0", 50);
    }

    #[test]
    #[should_panic]
    fn test_from_str_radix_large() {
        let _ = BigInt::from_str("ZYXWVUTSRQPONMLKJIHGFEDCBA987654321", 36);
    }

    #[test]
    fn test_from_str_radix_small() {
        let num = BigInt::from_str("FEDCBA", 16);
        assert!(num.inner[0] > 0, "Number is not greater than 0");
        assert!(num.inner[0] < usize::MAX, "Result is larger than usize");
        assert_eq!(num.inner[0], 0xFEDCBA_usize);
    }

    #[test]
    fn test_from_str_radix_lowercase() {
        let num = BigInt::from_str("fedcba", 16);
        assert_eq!(num.inner[0], 0xFEDCBA_usize);
    }
}
