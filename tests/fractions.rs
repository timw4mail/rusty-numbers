#[cfg(not(tarpaulin_include))]
mod tests {
    use rusty_numbers::frac;
    use rusty_numbers::rational::Frac;

    #[test]
    fn mul_test() {
        let frac1 = frac!(1 / 3u8);
        let frac2 = frac!(2u8 / 3);

        let expected = frac!(2u8 / 9);

        assert_eq!(frac1 * frac2, expected);

        assert_eq!(frac!(0), frac!(0) * frac!(5 / 8));
    }

    #[test]
    fn div_test() {
        assert_eq!(frac!(1), frac!(1 / 3) / frac!(1 / 3));
        assert_eq!(frac!(1 / 9), frac!(1 / 3) / frac!(3));
    }

    #[test]
    fn add_test() {
        assert_eq!(frac!(5 / 8), frac!(5 / 8) + frac!(0));
        assert_eq!(frac!(5 / 6), frac!(1 / 3) + frac!(1 / 2), "1/3 + 1/2");
        assert_eq!(frac!(1 / 3), frac!(2 / 3) + -frac!(1 / 3), "2/3 + -1/3");
    }

    #[test]
    fn sub_test() {
        assert_eq!(frac!(5 / 8), frac!(5 / 8) - frac!(0));
        assert_eq!(frac!(1 / 6), frac!(1 / 2) - frac!(1 / 3), "1/2 - 1/3");
    }

    #[test]
    fn cmp_test() {
        assert!(frac!(1 / 2) <= frac!(1 / 2));
        assert!(frac!(0) < frac!(1));
        assert!(-frac!(5 / 3) < frac!(1 / 10_000));
        assert!(frac!(1 / 10_000) > -frac!(10));
        assert!(frac!(1 / 3) < frac!(1 / 2));
        assert_eq!(frac!(1 / 2), frac!(3 / 6));
    }

    #[test]
    fn negative_fractions() {
        assert_eq!(-frac!(1 / 3), -frac!(2 / 3) + frac!(1 / 3), "-2/3 + 1/3");
        assert_eq!(frac!(1), frac!(1 / 3) - -frac!(2 / 3), "1/3 - -2/3");
        assert_eq!(-frac!(1), -frac!(2 / 3) - frac!(1 / 3), "-2/3 - +1/3");
        assert_eq!(-frac!(1), -frac!(2 / 3) + -frac!(1 / 3), "-2/3 + -1/3");
    }

    #[test]
    fn negative_mul_div() {
        assert_eq!(-frac!(1 / 12), -frac!(1 / 3) * frac!(1 / 4));
        assert_eq!(-frac!(1 / 12), frac!(1 / 3) * -frac!(1 / 4));
        assert_eq!(frac!(1 / 12), -frac!(1 / 3) * -frac!(1 / 4));
    }

    #[test]
    #[should_panic(expected = "Fraction can not have a zero denominator")]
    fn zero_denom() {
        frac!(1 / 0);
    }

    #[test]
    fn fraction_reducing() {
        assert_eq!(frac!(1u8 / 2), Frac::new_unreduced(48u8, 96u8).reduce());
    }

    #[test]
    fn op_assign() {
        // Addition
        let mut quart = frac!(1 / 4);
        quart += frac!(1 / 4);
        assert_eq!(frac!(1 / 2), quart);

        // Subtraction
        let mut half = frac!(1 / 2);
        half -= frac!(1 / 4);
        assert_eq!(frac!(1 / 4), half);

        // Multiplication
        let mut half = frac!(1 / 2);
        half *= frac!(1 / 2);
        assert_eq!(frac!(1 / 4), half);

        // Division
        let mut quart = frac!(1 / 4);
        quart /= frac!(4);
        assert_eq!(frac!(1 / 16), quart);
    }
}
