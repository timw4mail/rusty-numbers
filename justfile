# Lists the available actions
default:
	@just --list

# Generate test coverage report. Only works on amd64 linux, and requires pycobertura to be installed, as well as cargo-tarpaulin
coverage:
	cargo tarpaulin --out Xml
	pycobertura show --format html --output coverage.html cobertura.xml

# Remove generated files
clean:
	cargo clean
	rm -f cobertura.xml
	rm -f coverage.html

# Check code syntax
check:
	cargo check

# Generate API docs
docs:
	cargo doc

# Check code style
lint:
	cargo clippy

# Format the code
fmt:
	cargo +nightly fmt

# Automatically fix some code syntax/style issues
fix:
	cargo fix --allow-dirty --allow-staged
	just fmt

# Run the normal tests
test:
	cargo test

# Run tests without the standard library
test-no-std:
	cargo test --no-default-features --features alloc