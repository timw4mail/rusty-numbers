# Rusty Numbers

Playing around with numeric types in Rust.

[![Build Status](https://jenkins.timshome.page/buildStatus/icon?job=timw4mail%2Frusty-numbers%2Fmaster)](https://jenkins.timshome.page/job/timw4mail/job/rusty-numbers/job/master/)

## Components

* Rational (fraction) type, which overloads arithmatic operators
* BigInt (high precision integer) type, overloading arithmatic operators